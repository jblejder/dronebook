﻿using System.Collections.ObjectModel;
using DroneBook.BusinessLogic.Services;

namespace DroneBook.BusinessLogic.UnitTests.Dummies
{
    using Repositories;

    internal class DummyDroneRepository : IDroneRepository
    {
        public Collection<Drone> GetDrones()
        {
            return new Collection<Drone>
            {
                new Drone {Name = "0"},
                new Drone {Name = "1"},
                new Drone {Name = "2"}
            };
        }

        public void CreateDrone(Drone drone)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateDrone(int id, Drone drone)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteDrone(int id)
        {
            throw new System.NotImplementedException();
        }

        public Drone GetDrone(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
