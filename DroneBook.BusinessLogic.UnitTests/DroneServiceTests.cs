﻿namespace DroneBook.BusinessLogic.UnitTests
{
    using System;
    using System.Collections.ObjectModel;
    using Services;
    using Dummies;
    using Moq;
    using NUnit.Framework;
    using Repositories;

    [TestFixture]
    class DroneServiceTests
    {
        [Test]
        public void Ctor_throws_exception_when_service_argument_is_null()
        {
            //Act & Assert
            Assert.Throws<ArgumentNullException>(() => new DroneService(null));
        }

        [Test]
        public void GetAllDrones_should_return_all_drones_from_repository_mock_case()
        {
            // Arrange
            Mock<IDroneRepository> droneRepositoryMock = new Mock<IDroneRepository>();
            droneRepositoryMock.Setup(p => p.GetDrones()).Returns(new Collection<Drone>
            {
                new Drone {Name = "0"},
                new Drone {Name = "1"},
                new Drone {Name = "2"}
            });
            IDroneService service = new DroneService(droneRepositoryMock.Object);

            // Act
            var drones = service.GetAllDrones();

            // Assert
            Assert.AreEqual(3, drones.Count);
            Assert.AreEqual("0", drones[0].Name, "Invalid name of first element");
            Assert.AreEqual("2", drones[2].Name, "Invalid name of last element");
        }

        [Test]
        public void GetAllDrones_should_return_all_drones_from_repository_dummy_case()
        {
            // Arrange
            IDroneService service = new DroneService(new DummyDroneRepository());

            // Act
            var drones = service.GetAllDrones();

            // Assert
            Assert.AreEqual(3, drones.Count);
            Assert.AreEqual("0", drones[0].Name, "Invalid name of first element");
            Assert.AreEqual("2", drones[2].Name, "Invalid name of last element");
        }
    }
}
