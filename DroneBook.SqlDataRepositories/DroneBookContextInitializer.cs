﻿namespace DroneBook.SqlDataRepositories
{
    using System.Data.Entity;

    internal class DroneBookContextInitializer : DropCreateDatabaseIfModelChanges<DroneBookContext>
    {
        protected override void Seed(DroneBookContext context)
        {
            // TODO hardcoded values
            context.Drones.Add(new DroneEntity()
            {
                Name = "Biedronka",
                Description = "Czerwona"
            });

            context.Drones.Add(new DroneEntity()
            {
                Name = "Stonka",
                Description = "Czarna"
            });

            context.Drones.Add(new DroneEntity()
            {
                Name = "Pajonk",
                Description = "Z dlugimi nogami"
            });
        }
    }
}