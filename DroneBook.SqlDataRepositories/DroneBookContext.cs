﻿namespace DroneBook.SqlDataRepositories
{
    using System.Data.Entity;
    using System.Reflection.Emit;
    using BusinessLogic.Services;

    internal class DroneBookContext : DbContext
    {
        public DroneBookContext() : base("DroneBookSQL")
        {
            Database.SetInitializer(new DroneBookContextInitializer());
        }

        public DbSet<DroneEntity> Drones { get; set; }
    }

    internal class DroneEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }

        public static explicit operator Drone(DroneEntity drone)
        {
            var result = new Drone()
            {
                Id = drone.Id,
                Name = drone.Name,
                Description = drone.Description,
                Type = drone.Type,
                Size = drone.Size
            };
            return result;
        }

        public static explicit operator DroneEntity(Drone drone)
        {
            var result = new DroneEntity()
            {
                Id = drone.Id,
                Name = drone.Name,
                Description = drone.Description,
                Type = drone.Type,
                Size = drone.Size
            };
            return result;
        }
    }
}
