﻿using System.Linq;

namespace DroneBook.SqlDataRepositories
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using BusinessLogic.Repositories;
    using BusinessLogic.Services;

    public class DroneRepository : IDroneRepository
    {
        public Collection<Drone> GetDrones()
        {
            var coll = new Collection<Drone>();
            using (var context = new DroneBookContext())
            {
                foreach (var drone in context.Drones)
                {
                    coll.Add((Drone)drone);
                }
            }
            return coll;
        }

        public void CreateDrone(Drone drone)
        {
            using (var context = new DroneBookContext())
            {
                context.Drones.Add((DroneEntity)drone);
                context.SaveChanges();
            }
        }

        public void UpdateDrone(int id, Drone drone)
        {
            if (id != drone.Id)
                return;
            using (var context = new DroneBookContext())
            {
                var droneEntity = context.Drones.FirstOrDefault(d => d.Id == id);
                if (droneEntity == null)
                    return;
                droneEntity = (DroneEntity)drone;
                context.Entry(droneEntity);
                context.SaveChanges();
            }
        }


        public void DeleteDrone(int id)
        {
            using (var context = new DroneBookContext())
            {
                var drone = context.Drones.SingleOrDefault(x => x.Id == id);
                context.Drones.Remove(drone);
                context.SaveChanges();
            }
        }

        public Drone GetDrone(int id)
        {
            DroneEntity drone;
            using (var context = new DroneBookContext())
            {
                drone = context.Drones.FirstOrDefault<DroneEntity>(d => d.Id == id);
            }
            if (drone != null)
                return (Drone)drone;

            return null;
        }
    }
}
