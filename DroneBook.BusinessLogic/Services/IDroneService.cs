﻿using System.Collections.ObjectModel;

namespace DroneBook.BusinessLogic.Services
{
    public interface IDroneService
    {
        Collection<Drone> GetAllDrones();

        void CreateDrone(Drone drone);
        void DeleteDrone(int id);
        Drone GetDrone(int id);
    }
}