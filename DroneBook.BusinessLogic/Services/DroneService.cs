﻿namespace DroneBook.BusinessLogic.Services
{
    using System;
    using System.Collections.ObjectModel;
    using Repositories;

    public class DroneService : IDroneService
    {
        private readonly IDroneRepository repository;

        public DroneService(IDroneRepository repository)
        {
            if (repository == null)
            {
                throw new ArgumentNullException(nameof(repository));
            }
            this.repository = repository;
        }

        public Collection<Drone> GetAllDrones()
        {
            return repository.GetDrones();
        }

        public void CreateDrone(Drone drone)
        {
            repository.CreateDrone(drone);
        }


        public void DeleteDrone(int id)
        {
            repository.DeleteDrone(id);
        }

        public Drone GetDrone(int id)
        {
            return repository.GetDrone(id);
        }
    }
}
