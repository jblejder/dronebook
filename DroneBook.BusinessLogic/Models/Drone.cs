﻿namespace DroneBook.BusinessLogic.Services
{
    public class Drone
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
    }
}