﻿namespace DroneBook.BusinessLogic.Repositories
{
    using System.Collections.ObjectModel;
    using Services;

    public interface IDroneRepository
    {
        Collection<Drone> GetDrones();
        void CreateDrone(Drone drone);
        void UpdateDrone(int id, Drone drone);
        void DeleteDrone(int id);
        Drone GetDrone(int id);
    }
}
