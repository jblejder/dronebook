﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DroneBook.BusinessLogic.Repositories;
using DroneBook.BusinessLogic.Services;
using DroneBook.SqlDataRepositories;
using Ninject;

namespace DroneBook.WebUI.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        public IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null
                ? null
                : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            ninjectKernel.Bind<IDroneService>().To<DroneService>();
            ninjectKernel.Bind<IDroneRepository>().To<DroneRepository>();
        }
    }
}