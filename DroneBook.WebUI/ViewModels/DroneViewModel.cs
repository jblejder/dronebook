﻿using DroneBook.BusinessLogic.Services;

namespace DroneBook.WebUI.ViewModels
{
    public class DroneViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }

        public DroneViewModel()
        {            
        }

        public DroneViewModel(Drone drone)
        {
            this.Name = drone.Name;
            this.Description = drone.Description;
            this.Type = drone.Type;
            Size = drone.Size;
        }
    }
}