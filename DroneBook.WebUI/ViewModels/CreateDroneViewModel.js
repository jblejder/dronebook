﻿var CreateDroneViewModel = {
    Drone: {
        Name: ko.observable("name"),
        Description: ko.observable("description"),
        Type: ko.observable("quad"),
        Size: ko.observable("250"),
        Controller: ko.observable('controller'),
        ESC: ko.observable('esc'),
        Rotors: ko.observable('rotors'),
        Receiver: ko.observable('receiver'),
        Battery: ko.observable('battery'),
        Camera: ko.observable('camera'),
        Gimbal: ko.observable('gimbal'),
        OSD: ko.observable('osd'),
        VideoRX: ko.observable('videorx'),
        VideoTX: ko.observable('videotx'),
        Additional: ko.observable('additional')
    },

    currentPage: ko.observable(0),

    NextPage: function() {
        this.currentPage(this.currentPage() + 1);
        if (this.currentPage() > 1) {
            document.getElementById('files').addEventListener('change', this.handleFileSelect, false);
        }
    },
    PreviousPage: function() {
        this.currentPage(this.currentPage() - 1);
    },

    Sending: false,

    Send: function () {
        if (this.Sending === true)
            return;
        $("#SendButton").attr("disabled", true);
        this.Sending = true;
        var postUrl = "http://localhost:50644/api/CreateDroneApi/post";
        $.ajax(postUrl, {
            data: ko.toJSON(this.Drone),
            type: "post",
            contentType: "application/json",
            success: function () {
                confirm("Twój dron został dodany poprawnie!");
                window.location = "/";
            },
            error: function () {
                confirm("Nie udało się dodać");
            }
        });
    },

    handleFileSelect: function (evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; (f = files[i]) ; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                                      '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

};

//document.getElementById('files').addEventListener('change', ViewModel.handleFileSelect, false);


$(window).ready(function () {
    var counter = $(".PartItem").length;
    var partP = $('#part').clone();

    console.log("document loaded");

    $("#AddField").click(function () {
        partP.children().eq(0).attr("id", "Parts_" + counter + "__Name");
        partP.children().eq(0).attr("Name", "Parts[" + counter + "].Name");
        partP.children().eq(1).attr("id", "Parts_" + counter + "__PartType");
        partP.children().eq(1).attr("Name", "Parts[" + counter + "].PartType");
        partP.children().eq(1).removeAttr("style");
        counter++;
        $("#PartsList").append(partP);

        partP = $('#part').clone();
    });


});

ko.bindingHandlers.slideToggle = {
    init: function (element, valueAccessor) {
        // Initially set the element to be instantly visible/hidden depending on the value
        var value = valueAccessor();
        $(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
    },
    update: function (element, valueAccessor) {
        // Whenever the value subsequently changes, slowly slide the element down or up
        var value = valueAccessor();
        ko.utils.unwrapObservable(value) ? $(element).slideDown() : $(element).slideUp('fast');

    }
};

ko.applyBindings(CreateDroneViewModel);