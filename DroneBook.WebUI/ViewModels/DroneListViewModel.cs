﻿namespace DroneBook.WebUI.ViewModels
{
    using System.Collections.Generic;

    public class DroneListViewModel
    {
        private List<DroneViewModel> _drones;

        public List<DroneViewModel> Drones
        {
            get
            {
                if (_drones == null)
                {
                    _drones = new List<DroneViewModel>();
                } 

                return _drones;
            }
            set { _drones = value; }
        }
    }
}