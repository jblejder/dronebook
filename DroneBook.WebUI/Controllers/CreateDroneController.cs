﻿namespace DroneBook.WebUI.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using BusinessLogic.Services;
    using WebApi.ViewModel;


    public class CreateDroneController : Controller
    {

        public ViewResult Create()
        {
            var model = new CreateDroneViewModel();
            model.Parts = new List<PartsViewModel>()
            {
                new PartsViewModel() {PartType = PartType.Controller},
                new PartsViewModel() {PartType = PartType.ESC},
                new PartsViewModel() {PartType = PartType.Rotors},
                new PartsViewModel() {PartType = PartType.Props},
                new PartsViewModel() {PartType = PartType.Receiver},
            };
            return View(model);
        }

    }
}