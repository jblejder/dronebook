﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DroneBook.WebUI.Controllers
{
    using System.ComponentModel;
    using Newtonsoft.Json;

    public class ProbyController : Controller
    {
        // GET: Proby
        public ActionResult Index()
        {
            ProbyViewModel model = new ProbyViewModel()
            {
                Names = new List<Complextype>()
                {
                    new Complextype()
                    {
                        Name = "Kuba",
                        Type = TypesViewModel.Pierwsa
                    },
                    new Complextype()
                    {
                        Name = "Maciek",
                        Type = TypesViewModel.Druga
                    }
                }
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ProbyViewModel model)
        {
            return View(model);
        }
    }

    public class ProbyViewModel
    {
        public string Text { get; set; }
        public List<Complextype> Names { get; set; }
    }

    public class Complextype
    {
        public string Name { get; set; }
        public TypesViewModel Type { get; set; }
    }

    public enum TypesViewModel
    {
        [Description("cos1")]
        Pierwsa=1,
        [Description("cos2")]
        Druga
    }
}