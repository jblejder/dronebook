﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DroneBook.BusinessLogic.Services;
using DroneBook.WebUI.ViewModels;

namespace DroneBook.WebUI.Controllers
{
    using System.Net.Http;
    using Infrastructure;
    using Newtonsoft.Json;

    public class DroneController : Controller
    {
        public ActionResult List()
        {
            return View();
        }

        public ActionResult Detail(int id)
        {
            HttpClient client = new HttpClient();
            string json = client.GetStringAsync(new Uri(Constants.WebApiAddres + id)).Result;
            Drone drone = JsonConvert.DeserializeObject<Drone>(json);

            return View(drone);
        }
    }
}