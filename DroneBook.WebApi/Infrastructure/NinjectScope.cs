﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DroneBook.WebApi.Infrastructure
{
    using System.Web.Http.Dependencies;
    using BusinessLogic.Repositories;
    using BusinessLogic.Services;
    using Ninject;
    using Ninject.Activation;
    using Ninject.Parameters;
    using Ninject.Syntax;
    using SqlDataRepositories;

    public class NinjectScope : IDependencyScope
    {
        protected IResolutionRoot resolutionRoot;

        public NinjectScope(IResolutionRoot kernel)
        {
            resolutionRoot = kernel;
        }

        public object GetService(Type serviceType)
        {
            IRequest request = resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return resolutionRoot.Resolve(request).SingleOrDefault();
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            IRequest request = resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return resolutionRoot.Resolve(request).ToList();
        }

        public void Dispose()
        {
            IDisposable disposable = (IDisposable)resolutionRoot;
            if (disposable != null) disposable.Dispose();
            resolutionRoot = null;
        }
    }

    public class NinjectResolver : NinjectScope, IDependencyResolver
    {
        private readonly IKernel _kernel;
        public NinjectResolver(IKernel kernel) : base(kernel)
        {
            _kernel = kernel;
            AddBindings();
        }
        public IDependencyScope BeginScope()
        {
            return new NinjectScope(_kernel.BeginBlock());
        }

        private void AddBindings()
        {
            _kernel.Bind<IDroneService>().To<DroneService>();
            _kernel.Bind<IDroneRepository>().To<DroneRepository>();
        }
    }
}