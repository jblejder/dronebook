﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace DroneBook.WebApi
{
    using BusinessLogic.Repositories;
    using BusinessLogic.Services;
    using Infrastructure;
    using Ninject;
    using SqlDataRepositories;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var kernel = new StandardKernel();
            kernel.Bind<IDroneService>().To<DroneService>();
            kernel.Bind<IDroneRepository>().To<DroneRepository>();
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(new StandardKernel());
        }
    }
}
