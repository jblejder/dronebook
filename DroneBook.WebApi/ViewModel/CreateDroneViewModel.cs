﻿namespace DroneBook.WebApi.ViewModel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class CreateDroneViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        [Range(0,11)]
        public List<PartsViewModel> Parts { get; set; }

        public List<byte> Images { get; set; }
    }

    public enum PartType
    {
        Controller,
        ESC,
        Rotors,
        Props,
        Receiver,
        Battery,
        Camera,
        Gimbal,
        OSD,
        VideoRX,
        VideoTX,
        Additional
    }

    public class PartsViewModel
    {
        public string Name { get; set; }
        public PartType PartType { get; set; }
    }
}