﻿namespace DroneBook.WebApi.Controllers
{
    using System.Web.Http;
    using System.Web.Http.Cors;
    using BusinessLogic.Services;
    using Newtonsoft.Json;
    using ViewModel;

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CreateDroneApiController : ApiController
    {
        IDroneService service;

        public CreateDroneApiController(IDroneService service)
        {
            this.service = service;
        }

        // GET: api/CreateDroneApi
        public string Get()
        {
            var data = service.GetAllDrones();
            return JsonConvert.SerializeObject(data);
        }

        // GET: api/CreateDroneApi/5
        public Drone Get(int id)
        {
            var data = service.GetDrone(id);
            return data;
        }

        // POST: api/CreateDroneApi
        [HttpPost]
        public void Post(CreateDroneViewModel model)
        {
            Drone drone = new Drone();
            drone.Name = model.Name;
            drone.Description = model.Description;
            drone.Type = model.Type;
            drone.Size = model.Size;

            service.CreateDrone(drone);
        }

        // PUT: api/CreateDroneApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CreateDroneApi/5
        public void Delete(int id)
        {
            service.DeleteDrone(id);
        }
    }
}
