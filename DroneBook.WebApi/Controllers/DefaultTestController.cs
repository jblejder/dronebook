﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DroneBook.WebApi.Controllers
{
    using System.Web.Http.Cors;

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DefaultTestController : ApiController
    {
        // GET: api/DefaultTest
        public IEnumerable<string> Get()
        {
            return new string[] { "Hello", "from", "WebApi!" };
        }

        // GET: api/DefaultTest/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/DefaultTest
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/DefaultTest/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DefaultTest/5
        public void Delete(int id)
        {
        }
    }
}
